package ctci.ArraysAndStrings;

import java.util.Arrays;

public class CheckPermutation {

    /**
     * Determines if str1 is a permutation of str2.
     *
     * @param str1 the first string.
     * @param str2 the second string.
     * @return true if str1 is a permutation of str2, false otherwise.
     */
    boolean checkPermutation(String str1, String str2) {
        /**
         * If str1 is a permutation of str2, they should
         * have the exact same characters. Hence,
         * we can simply sort the String and compare them
         * character by character to determine that.
         *
         * Time Complexity: O(n)
         */
        if (str1.length() != str2.length()) {
            return false;
        }
        char[] s1Chars = str1.toCharArray();
        char[] s2Chars = str2.toCharArray();
        Arrays.sort(s1Chars);
        Arrays.sort(s2Chars);
        for (int i = 0; i < str1.length(); i++) {
            if (s1Chars[i] != s2Chars[i]) {
                return false;
            }
        }
        return true;
    }

}
