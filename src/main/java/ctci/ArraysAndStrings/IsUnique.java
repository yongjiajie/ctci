package ctci.ArraysAndStrings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class IsUnique {

    /**
     * Determines if a string has all unique characters.
     *
     * @return true if string has all unique characters, false otherwise.
     */
    boolean isUnique(String string) {
        /**
         * We can simply use a HashMap to keep track of characters
         * that we have already encountered. If a key exists,
         * a duplicate character has been found.
         *
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         */
        Map<Character, Integer> characterCountMap = new HashMap<>();
        for (int i = 0; i < string.length(); i++) {
            Character currentChar = string.charAt(i);
            if (characterCountMap.containsKey(currentChar)) {
                return false;
            } else {
                characterCountMap.put(currentChar, 1);
            }
        }
        return true;
    }

    /**
     * Determines if a string has all unique characters without using data structures.
     *
     * @return true if string has all unique characters, false otherwise.
     */
    boolean isUniqueNoDataStructures(String string) {
        /**
         * We can simply sort the string and iterate character
         * by character and check if there are duplicates.
         */
        char[] stringChars = string.toCharArray();
        Arrays.sort(stringChars);
        for (int i = 1; i < string.length(); i++) {
            if (stringChars[i - 1] == stringChars[i]) {
                return false;
            }
        }
        return true;
    }

}
