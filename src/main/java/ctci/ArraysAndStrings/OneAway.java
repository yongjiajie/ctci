package ctci.ArraysAndStrings;

public class OneAway {

    /**
     * Check if str1 is one edit away from str2.
     * Edits can be: inserting a character, removing a character, or replacing a character.
     *
     * @return true if str1 is one edit away from str2, false otherwise.
     */
    boolean isOneAway(String str1, String str2) {
        /**
         * There are three possible scenarios that can be split
         * based on the length of both strings.
         *
         * Runtime Complexity: O(n)
         */
        if (str1.length() == str2.length()) {
            return isOneReplaceAway(str1, str2);
        } else if (str1.length() < str2.length()) {
            return isOneEditAway(str1, str2);
        } else {
            return isOneEditAway(str2, str1);
        }
    }

    boolean isOneReplaceAway(String str1, String str2) {
        /**
         * We simply compare both strings character by character.
         * When we find a character in one string that is not in the
         * other, we increment a count. If that count exceeds 1,
         * return false. Otherwise, return true.
         */
        int replaceCount = 0;
        for (int i = 0; i < str1.length() && replaceCount < 2; i++) {
            if (str1.charAt(i) != str2.charAt(i)) {
                replaceCount++;
            }
        }
        return replaceCount == 1;
    }

    boolean isOneEditAway(String str1, String str2) {
        /**
         * We compare both strings character by character, incrementing
         * the index of the shorter string by 1 when an edit is found.
         * Once the count exceeds 1, return false. Otherwise, return true.
         */
        int editCount = 0;
        for (int i = 0; i < str1.length() && editCount < 2; i++) {
            if (str1.charAt(i) != str2.charAt(i + editCount)) {
                editCount++;
            }
        }
        return editCount < 2;
    }

}
