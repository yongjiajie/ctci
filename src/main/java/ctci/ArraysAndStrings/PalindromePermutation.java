package ctci.ArraysAndStrings;

import java.util.HashMap;
import java.util.Map;

public class PalindromePermutation {

    /**
     * Check if string is a permutation of a palindrome.
     *
     * @return true if string is a permutation of a palindrome, false otherwise.
     */
    boolean isPalindromePermutation(String str) {
        /**
         * A palindrome can only contain 0 or 1 characters with odd counts
         * in the string. We keep track of the character counts using a HashMap.
         *
         * If the character count is odd, we increment a counter, and vice versa
         * if the character count is even. This allows us to count while iterating
         * through the string, instead of wasting time iterating through the HashMap
         * later.
         *
         * Finally, we can simply return true is the counter is below 2.
         *
         * Time Complexity: O(n)
         * Space Complexity: O(n)
         *
         */
        int oddCount = 0;
        Map<Character, Integer> charCountMap = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char currChar = str.charAt(i);
            if (currChar != ' ') {
                int count = charCountMap.getOrDefault(currChar, 0);
                charCountMap.put(currChar, ++count);
                if (count % 2 == 1) {
                    oddCount++;
                } else {
                    oddCount--;
                }
            }
        }
        return oddCount < 2;
    }

}
