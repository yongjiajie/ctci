package ctci.ArraysAndStrings;

public class RotateMatrix {

    /**
     * Rotates matrix in place 90 degrees clockwise.
     *
     * @param matrix a 2D matrix.
     * @return the rotated matrix.
     */
    int[][] rotateMatrix(int[][] matrix) {
        /**
         * We rotate the matrix layer by layer, starting from the outer.
         *
         * At each iteration, we save the ith item at the top.
         * Then, we move items clockwise, starting from left to top,
         * bottom to left, right to bottom, then finally top to right.
         * Take note to only take n-1 items from each side of the layer
         * so you don't overwrite items.
         *
         * The difficulty in this solution is making sure your indexes are pointing
         * to where you want them to.
         *
         * Time Complexity: O(n)
         * Space Complexity: O(1/4 of items in each layer)
         */
        if (matrix.length == 0 || matrix.length != matrix[0].length) {
            return matrix;
        }
        for (int i = 0; i < matrix.length / 2; i++) {
            int firstIndex = i;
            int lastIndex = matrix.length - 1 - i;
            for (int j = firstIndex; j < lastIndex; j++) {
                // Save top item
                int top = matrix[i][j];
                // Move left to top
                matrix[i][j] = matrix[lastIndex - j - firstIndex][j];
                // Move bottom to left
                matrix[lastIndex - j - firstIndex][j] = matrix[lastIndex][lastIndex - j - firstIndex];
                // Move right to bottom
                matrix[lastIndex][lastIndex - j - firstIndex] = matrix[j][lastIndex];
                // Move top to right
                matrix[j][lastIndex] = top;
            }
        }
        return matrix;
    }

}
