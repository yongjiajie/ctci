package ctci.ArraysAndStrings;

public class StringCompression {

    /**
     * Performs basic string compression using counts of repeated characters in the string.
     * If the compressed string is not smaller than the original string, the original string will be returned.
     *
     * @param str
     * @return
     */
    String compress(String str) {
        /**
         * We use a StringBuilder to easily manipulate the final string.
         * We check every character, incrementing a counter if they
         * are the same as the previous, and resetting the counter if they are not.
         * When the counter is reset, we also output the compressed character with its count
         * to the final string.
         *
         * One catch is that the checking loop will terminate without compressing the final
         * character, which has to be handled.
         *
         * Time Complexity: O(n)
         * Space Complexity: O(n) for the StringBuilder
         */
        StringBuilder stringBuilder = new StringBuilder();
        int count = 1;
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i - 1) != str.charAt(i)) {
                stringBuilder.append(str.charAt(i - 1));
                stringBuilder.append(count);
                count = 1;
            } else {
                count++;
            }
        }
        stringBuilder.append(str.charAt(str.length() - 1));
        stringBuilder.append(count);
        return (stringBuilder.length() < str.length()) ? stringBuilder.toString() : str;
    }

}
