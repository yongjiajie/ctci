package ctci.ArraysAndStrings;

public class StringRotation {

    /**
     * Checks if str2 is a rotation of str1 using only ONE call to `isSubstring`.
     *
     * @param str1 the first String.
     * @param str2 the second String.
     * @return true if str2 is a rotation of str1, otherwise false.
     */
    static boolean isStringRotation(String str1, String str2) {
        /**
         * If str2 is a rotation of str1, it will contain str1 if
         * str2 is concatenated with itself. Therefore, we simply
         * concatenate str2 with itself and use isSubstring.
         */
        str2 = str2 + str2;
        if (isSubstring(str1, str2)) {
            return true;
        }
        return false;
    }

    static boolean isSubstring(String str1, String str2) {
        return str2.contains(str1);
    }

}
