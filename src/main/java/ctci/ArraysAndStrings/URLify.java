package ctci.ArraysAndStrings;

public class URLify {

    /**
     * Replace all spaces in a string with '%20'.
     *
     * @param strChars array of characters in the string.
     * @param length   length of string.
     * @return string with all spaces replaced with '%20'.
     */
    String URLify(char[] strChars, int length) {
        /**
         * Since we are given the actual length of the string,
         * we can iterate through the string and process it.
         *
         * We iterate through the string backwards so we can do
         * the process in-place and not overwrite the original
         * string.
         *
         * If the current character is not a space, we simply move
         * it to the end. Else, we replace it with '%20' at the
         * end and move the index as necessary.
         *
         * Time Complexity: O(n)
         */
        for (int i = strChars.length - 1; i > 0; i--) {
            if (strChars[length - 1] == ' ') {
                strChars[i] = '0';
                strChars[i - 1] = '2';
                strChars[i - 2] = '%';
                i -= 2;
            } else {
                strChars[i] = strChars[length - 1];
            }
            length--;
        }
        return String.valueOf(strChars);
    }

}
