package ctci.ArraysAndStrings;

public class ZeroMatrix {

    /**
     * If an element is zero, its entire row and column are set to 0.
     *
     * @param matrix the 2D matrix.
     * @return the zeroed matrix.
     */
    static int[][] zeroMatrix(int[][] matrix) {
        /**
         * The idea is to use the first row and columns to mark
         * whether a row or column should be zeroed.
         *
         * Time Complexity: O(n^2) since we have to iterate twice,
         *                  once to check the elements, another
         *                  to zero them.
         *
         * Space Complexity: O(n) since we use the matrix itself to
         *                   determine whether the rows and columns
         *                   should be zeroed.
         *
         */
        boolean firstRowZeroed = false;
        boolean firstColumnZeroed = false;
        // We check if the first row and column has to be zeroed
        for (int i = 0; i < matrix[0].length; i++) {
            if (matrix[0][i] == 0) {
                firstRowZeroed = true;
            }
        }
        for (int i = 0; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                firstColumnZeroed = true;
            }
        }
        // We check if elements are zero, and zero the respective elements in the first row and column
        for (int row = 1; row < matrix.length; row++) {
            for (int column = 1; column < matrix[row].length; column++) {
                // If element is zero, mark first row and column element as zero
                if (matrix[row][column] == 0) {
                    matrix[0][column] = 0;
                    matrix[row][0] = 0;
                }
            }
        }
        // Zero elements based on first row and column value
        for (int i = 1; i < matrix.length; i++) {
            if (matrix[i][0] == 0) {
                zeroRow(matrix, i);
            }
            if (matrix[0][i] == 0) {
                zeroColumn(matrix, i);
            }
        }
        // Zero first row and column based on initial checks
        if (firstColumnZeroed) {
            zeroColumn(matrix, 0);
        }
        if (firstRowZeroed) {
            zeroRow(matrix, 0);
        }
        return matrix;
    }

    static void zeroRow(int[][] matrix, int row) {
        for (int column = 0; column < matrix[row].length; column++) {
            matrix[row][column] = 0;
        }
    }

    static void zeroColumn(int[][] matrix, int column) {
        for (int row = 0; row < matrix.length; row++) {
            matrix[row][column] = 0;
        }
    }

}
