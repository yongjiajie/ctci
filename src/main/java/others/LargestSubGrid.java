package others;

import java.util.List;

public class LargestSubGrid {

    public static int largestSubgrid(List<List<Integer>> grid, int maxSum) {
        int max = 0;
        for (int subgridSize = 1; subgridSize <= grid.size(); subgridSize++) {
            max = Math.max(getMaxFromSubgrid(grid, subgridSize), max);
        }
        return max;
    }

    static int getMaxFromSubgrid(List<List<Integer>> grid, int size) {
        int subgridMax = 0;
        for (int i = 0; i < grid.size() && i + size <= grid.size(); i++) {
            for (int j = 0; j < grid.size() && j + size <= grid.size(); j++) {
                int max = 0;
                for (int k = 0; k < size; k++) {
                    for (int l = 0; l < size; l++) {
                        max += grid.get(i + k).get(j + l);
                    }
                }
                subgridMax = Math.max(max, subgridMax);
            }
        }
        return subgridMax;
    }

}
