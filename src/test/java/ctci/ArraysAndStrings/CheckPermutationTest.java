package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CheckPermutationTest {

    private CheckPermutation checkPermutation = new CheckPermutation();

    @Test
    void checkPermutation() {
        assertTrue(checkPermutation.checkPermutation("aaa", "aaa"));
        assertTrue(checkPermutation.checkPermutation("waterbottle", "bottlewater"));
        assertFalse(checkPermutation.checkPermutation("abc", "abcd"));
    }
}