package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IsUniqueTest {

    private IsUnique isUnique = new IsUnique();

    @Test
    void isUnique() {
        assertTrue(isUnique.isUnique("abcd"));
        assertFalse(isUnique.isUnique("aa" ));
    }

    @Test
    void isUniqueNoDataStructures() {
        assertTrue(isUnique.isUniqueNoDataStructures("abcd"));
        assertFalse(isUnique.isUniqueNoDataStructures("aa" ));
    }
}