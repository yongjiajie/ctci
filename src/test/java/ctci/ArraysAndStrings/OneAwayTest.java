package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OneAwayTest {

    private OneAway oneAway = new OneAway();

    @Test
    void isOneAway() {
        assertTrue(oneAway.isOneAway("pale", "ple"));
        assertTrue(oneAway.isOneAway("pales", "pale"));
        assertTrue(oneAway.isOneAway("pale", "bale"));
        assertFalse(oneAway.isOneAway("pale", "bake"));
    }
}