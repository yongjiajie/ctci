package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromePermutationTest {

    private PalindromePermutation palindromePermutation = new PalindromePermutation();

    @Test
    void isPalindromePermutation() {
        assertTrue(palindromePermutation.isPalindromePermutation("tact coa"));
    }
}