package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringRotationTest {

    @Test
    void isStringRotation() {
        String str1 = "waterbottle";
        String str2 = "erbottlewat";
        boolean result = StringRotation.isStringRotation(str1, str2);
        assertTrue(result);
    }
}