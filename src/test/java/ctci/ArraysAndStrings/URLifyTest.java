package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class URLifyTest {

    private URLify urlify = new URLify();

    @Test
    void URLify() {
        String expected = "Mr%20John%20Smith";
        char[] strChars = "Mr John Smith    ".toCharArray();
        assertEquals(expected, urlify.URLify(strChars, 13));
    }
}