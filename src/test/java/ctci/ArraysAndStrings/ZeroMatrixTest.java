package ctci.ArraysAndStrings;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ZeroMatrixTest {

    @Test
    void zeroMatrix() {
        int[][] matrix = {{1, 2, 3, 4}, {5, 0, 7, 8}, {9, 10, 11, 12}};
        matrix = ZeroMatrix.zeroMatrix(matrix);
        System.out.println(Arrays.deepToString(matrix));
    }
}