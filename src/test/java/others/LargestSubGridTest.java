package others;

import org.junit.jupiter.api.Test;
import others.LargestSubGrid;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class LargestSubGridTest {

    @Test
    void largestSubgrid() {
        List<Integer> row1 = Arrays.asList(1, 1, 1);
        List<Integer> row2 = Arrays.asList(1, 1, 1);
        List<Integer> row3 = Arrays.asList(1, 1, 1);
        List<List<Integer>> grid = new ArrayList<>();
        grid.add(row1);
        grid.add(row2);
        grid.add(row3);

        int result = LargestSubGrid.largestSubgrid(grid, 4);
    }
}